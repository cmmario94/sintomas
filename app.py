DATABASE = 'symptoms.db'

def create_app():
    from flask import Flask, render_template
    from flask_bootstrap import Bootstrap
    from models import check_db

    app = Flask(__name__)
    # app.config['SECRET_KEY'] = 'mysecretkey'
    Bootstrap(app)

    from bp.api import api
    from bp.esp import esp
    from bp.sym import sym
    from bp.meds import meds
    from bp.treats import treats

    app.register_blueprint(api)
    app.register_blueprint(esp)
    app.register_blueprint(sym)
    app.register_blueprint(meds)
    app.register_blueprint(treats)
    
    check_db()

    @app.errorhandler(Exception)
    def all_exception_handler(error):
        return render_template('error.html', error=error)

    @app.route('/')
    def index():
        from database import Database
        
        db = Database()
        esp = db.query('SELECT * FROM esp')
        return render_template('calendar.html', esp=esp)

    @app.route('/table')
    def table():
        return render_template('table.html')
    
    return app

if __name__ == "__main__":
    app = create_app()
    app.run(debug=True)