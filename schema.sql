CREATE TABLE IF NOT EXISTS "esp" (
	"id"	INTEGER NOT NULL UNIQUE,
	"name"	TEXT NOT NULL UNIQUE,
	"color"	TEXT NOT NULL UNIQUE,
	PRIMARY KEY("id" AUTOINCREMENT)
);

INSERT INTO esp (name, color) VALUES ("OTROS", "gray");

CREATE TABLE "symptom_entries" (
	"id"	INTEGER,
	"date"	TEXT NOT NULL,
	"symptom_id"	INTEGER NOT NULL,
	"desc"	TEXT,
	"esp"	INTEGER NOT NULL DEFAULT 1,
	FOREIGN KEY("symptom_id") REFERENCES "symptoms"("id"),
	FOREIGN KEY("esp") REFERENCES "esp"("id"),
	PRIMARY KEY("id")
);

CREATE TABLE symptoms (
  id INTEGER PRIMARY KEY,
  name TEXT NOT NULL UNIQUE
);

CREATE VIEW symptoms_view AS
  SELECT symptom_entries.id, symptom_entries.date,
	symptom_entries.esp AS esp,
	esp.color AS color,
    symptom_entries.symptom_id, symptoms.name,
	symptom_entries.desc
    FROM symptom_entries
	JOIN symptoms ON symptom_entries.symptom_id = symptoms.id
	LEFT JOIN esp ON symptom_entries.esp = esp.id