from flask import current_app
from app import DATABASE
import sqlite3

class Database():

    def __init__(self) -> None:
        self.db = DATABASE

    def dict_factory(self, cursor, row):
        d = {}
        for idx, col in enumerate(cursor.description):
            d[col[0]] = row[idx]
        return d
    
    def get_con(self):
        self.con = sqlite3.connect(self.db)
        self.con.row_factory = self.dict_factory
        self.con.execute('PRAGMA foreign_keys = ON;')

    def query(self, sql, values=None):
        self.get_con()
        cur = self.con.cursor()
        try:
            if values: cur.execute(sql, values)
            else: cur.execute(sql)
            data = cur.fetchall()
            cur.close()
            self.con.close()
            return data
        except Exception as e:
            cur.close()
            self.con.close()
            raise e

    def insert(self, table, dict):
        self.get_con()
        cur = self.con.cursor()
        sql = f'INSERT INTO {table} ({", ".join(dict.keys())}'
        sql += ') VALUES ('
        for element in dict:
            sql += '?, '
        sql = sql.rstrip(', ') + ');'
        try:
            cur.execute(sql, list(dict.values()))
            id = cur.lastrowid
            cur.close()
            self.con.commit()
            self.con.close()
            return id
        except Exception as e:
            cur.close()
            self.con.close()
            raise e

    def update(self, table, id, dict):
        self.get_con()
        cur = self.con.cursor()
        sql = f'UPDATE {table} SET '
        for key in dict.keys():
            sql += f'{key} = ?, '
        sql = sql.rstrip(', ')
        if id: sql += f' WHERE id = {int(id)};'
        else: sql += ' ;'
        try:
            cur.execute(sql, list(dict.values()))
            cur.close()
            self.con.commit()
            self.con.close()
        except Exception as e:
            cur.close()
            self.con.close()
            raise e

    def delete(self, sql, values):
        self.get_con()
        cur = self.con.cursor()
        try:
            cur.execute(sql, values)
            cur.close()
            self.con.commit()
            self.con.close()
        except Exception as e:
            cur.close()
            self.con.close()
            raise e
