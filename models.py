import sqlite3
from app import DATABASE

def connect_db():
    conn = sqlite3.connect(DATABASE)
    conn.row_factory = sqlite3.Row
    return conn
    
def init_db():
    db = connect_db()
    with open('schema.sql', 'r') as f:
        print('Inicializando schema.sql...')
        db.cursor().executescript(f.read())
    db.commit()
    db.close()
    print('Base de datos creada correctamente')

def check_db():
    from os import listdir

    if 'symptoms.db' not in listdir():
        print('Base de datos no encontrada. Creando nueva instancia....')
        init_db()
    else:
        print('Base de datos encontrada.')

def get_symptom_by_name(name):
    with connect_db() as conn:
        c = conn.cursor()
        c.execute('SELECT * FROM symptoms WHERE name = ?', (name,))
        return c.fetchone()
    
def get_symptom_by_id(id):
    with connect_db() as conn:
        c = conn.cursor()
        c.execute('SELECT * FROM symptoms WHERE id = ?', (id,))
        return c.fetchone()

def add_symptom(name):
    with connect_db() as conn:
        c = conn.cursor()
        c.execute('INSERT INTO symptoms (name) VALUES (?)', (name,))
        conn.commit()
        return c.lastrowid

def add_symptom_entry(date, esp, desc, symptom_id):
    with connect_db() as conn:
        c = conn.cursor()
        c.execute('INSERT INTO symptom_entries (date, esp, desc, symptom_id) VALUES (?, ?, ?, ?)', (date, esp, desc, symptom_id))
        id = c.lastrowid
        conn.commit()
        return id

def sym_form_info(id):
    with connect_db() as conn:
        c = conn.cursor()
        esp = c.execute('SELECT * FROM esp;')
        esp = [dict(x) for x in esp]

        c = conn.cursor()
        treats = c.execute('SELECT * FROM treats_view')
        treats = [dict(x) for x in treats]

        c = conn.cursor()
        sel_treats = c.execute('SELECT * FROM symptom_treats_view WHERE entry_id=?;', (id,))
        sel_treats = [dict(x) for x in sel_treats]

        c = conn.cursor()
        c.execute("SELECT * FROM symptoms_view WHERE id = ?", (id,))
        row = c.fetchone()
        return row, esp, treats, sel_treats

def get_med_by_name(name):
    from database import Database
    db = Database()
    return db.query('SELECT * FROM meds WHERE desc = ?', (name,))