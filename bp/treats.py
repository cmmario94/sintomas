from flask import Blueprint, jsonify, redirect, render_template, request, url_for
from database import Database
from models import get_med_by_name


treats = Blueprint('treats', __name__, url_prefix='/treats')

@treats.route('/', methods=['GET', 'POST'])
def index():
    db = Database()
    if request.method == 'POST':
        treats = db.query('SELECT * FROM treats_view')
        return jsonify(treats)
    else:
        meds = [x['desc'] for x in db.query('SELECT desc FROM meds')]
        symps = db.query('SELECT id, name, date FROM symptoms_view ORDER BY date desc;')
        return render_template('treats.html', meds=meds, symps=symps)

@treats.route('/add', methods=['POST'])
def add():
    db = Database()
    data = {
        'start': request.form['start'],
        'poso': request.form['poso'],
        'end': request.form['end'],
        'obs': request.form['obs']
    }
    med = get_med_by_name(request.form['med'])
    if len(med) > 0:
        data['med_id'] = med[0]['id']
    else:
        med_id = db.insert('meds', {'desc': request.form['med']})
        data['med_id'] = med_id
    treat_id = db.insert('treats',  data)
    
    # enlazar sintomas
    symps = request.form.getlist('symps')
    if len(symps) > 0:
        for t in symps:
            db.insert('symptom_treats', {'entry_id': t, 'treat_id': treat_id})
    return redirect(url_for('treats.index'))


@treats.route('/edit/<int:id>', methods=['GET','POST'])
def edit(id):
    db = Database()
    if request.method == 'POST':
        data = {
            'start': request.form['start'],
            'poso': request.form['poso'],
            'end': request.form['end'],
            'obs': request.form['obs']
        }
        med = get_med_by_name(request.form['med'])
        if len(med) > 0:
            data['med_id'] = med[0]['id']
        else:
            med_id = db.insert('meds', {'desc': request.form['med']})
            data['med_id'] = med_id
        
        db.update('treats', id, data)

        #actualizar tratamientos asociados
        db.delete('DELETE FROM symptom_treats WHERE treat_id=?', (id,))
        symps = request.form.getlist('symps')
        if len(symps) > 0:
            for t in symps:
                db.insert('symptom_treats', {'entry_id': t, 'treat_id': id})

        return redirect(url_for('treats.index'))
    else:
        meds = [x['desc'] for x in db.query('SELECT desc FROM meds')]
        row = db.query('SELECT * FROM treats_view WHERE id=?', (id,))[0]
        symps = db.query('SELECT id, name, date FROM symptoms_view ORDER BY date desc;')
        selected = [x['entry_id'] for x in db.query('SELECT * FROM symptom_treats_view WHERE treat_id=?;', (id,))]
        return render_template('treats.html', meds=meds, row=row, symps=symps, selected=selected)

@treats.route('/del/<int:id>')
def delete(id):
    db = Database()
    db.delete("DELETE FROM treats WHERE id = ?", (id,))
    return redirect(url_for('treats.index'))