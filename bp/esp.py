from flask import Blueprint, redirect, render_template, request, url_for
from models import connect_db
from database import Database

esp = Blueprint('esp', __name__, url_prefix='/esp')

@esp.route('/')
def index():
    db = Database()
    esp = db.query('SELECT * FROM esp')
    return render_template('esp.html', esp=esp)

@esp.route('/add', methods=['POST'])
def add():
    db = Database()

    name = request.form['name']
    color = request.form['color']
    db.insert('esp',  {'name': name, 'color': color})
    return redirect(url_for('esp.index'))

@esp.route('/edit/<int:id>', methods=['POST'])
def edit(id):
    db = Database()

    name = request.form['name']
    color = request.form['color']
    db.update('esp', id, {'name': name, 'color': color})
    return redirect(url_for('esp.index'))

@esp.route('/del/<int:id>')
def delete(id):
    db = Database()
    db.delete("DELETE FROM esp WHERE id = ?", (id,))
    return redirect(url_for('esp.index'))