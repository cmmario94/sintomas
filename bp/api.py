from flask import Blueprint

api = Blueprint('api', __name__, url_prefix='/api')

@api.route('get_symptoms')
def get_symptoms():
    from flask import jsonify, request
    from datetime import datetime
    from database import Database
    
    db = Database()
    if request.args.get('feed') == "table":
        symptoms = db.query('SELECT * FROM symptoms_view;')
        return jsonify(symptoms)
    elif request.args.get('feed') == "calendar":
        # convertimos las fechas a objetos datetime
        start = datetime.fromisoformat(request.args.get('start'))
        end = datetime.fromisoformat(request.args.get('end'))

        # hacemos una consulta a la base de datos para obtener los síntomas que ocurren entre las fechas
        query = "SELECT * FROM symptoms_view WHERE date BETWEEN ? AND ?"
        if 'esp' in request.args:
            esp = request.args.get('esp')
            query += " AND esp_id = ?"
            params = (start, end, esp)
        else:
            params = (start, end)
        symptoms = db.query(query, params)

        # devolvemos los síntomas en un formato que fullcalendar.js entiende
        events = []
        for symptom in symptoms:
            events.append({
                'id': symptom['id'],
                'title': symptom['name'],
                'start': datetime.strptime(symptom['date'], '%Y-%m-%dT%H:%M').isoformat(),
                'color': symptom['color'] if symptom['color'] else "gray"
            })
        return jsonify(events)

@api.route('/bkp/<t>')
def bkp(t):
    from flask import send_file
    if t == '204ef240-3fc1-423c-b33d-9c66170753fb':
        return send_file("symptoms.db", as_attachment=True)
    else:
        return 'Acceso denegado', 401