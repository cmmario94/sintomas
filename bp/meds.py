from flask import Blueprint, redirect, render_template, request, url_for
from database import Database

meds = Blueprint('meds', __name__, url_prefix='/meds')

@meds.route('/')
def index():
    db = Database()
    meds = db.query('SELECT * FROM meds')
    return render_template('meds.html', meds=meds)

@meds.route('/add', methods=['POST'])
def add():
    db = Database()
    db.insert('meds',  {'desc': request.form['desc']})
    return redirect(url_for('meds.index'))

@meds.route('/edit/<int:id>', methods=['POST'])
def edit(id):
    db = Database()
    
    db.update('meds', id, {'desc': request.form['desc']})
    return redirect(url_for('meds.index'))

@meds.route('/del/<int:id>')
def delete(id):
    db = Database()
    db.delete("DELETE FROM meds WHERE id = ?", (id,))
    return redirect(url_for('meds.index'))