from flask import Blueprint, redirect, render_template, request, url_for
import models
from database import Database

sym = Blueprint('sym', __name__)

@sym.route('/add', methods=['GET', 'POST'])
def add():
    db = Database()
    if request.method == 'POST':
        symptom = request.form['symptom']
        date = request.form['datetime']
        desc = request.form['desc']

        if 'esp' in request.form:
            esp = request.form['esp']
        else:
            esp = None
        
        # si existe el symptom recuperar su id, no añadirlo
        result = models.get_symptom_by_name(symptom)
        if result is None:
            id = models.add_symptom(symptom)
        else:
            id = result['id']

        entry_id = models.add_symptom_entry(date, esp, desc, id)
        treats = request.form.getlist('treats')
        if len(treats) > 0:
            for t in treats:
                db.insert('symptom_treats', {'entry_id': entry_id, 'treat_id': t})
        return redirect(url_for('index'))
    else:
        # Consultar la base de datos para obtener los síntomas existentes
        symptoms = [x['name'] for x in db.query('SELECT name FROM symptoms;')]
        treats = db.query('SELECT * FROM treats_view ORDER BY desc, start desc')
        esp = db.query('SELECT * FROM esp;')
    
        return render_template('edit.html', symptoms=symptoms, esp=esp, treats=treats)

@sym.route('/edit/<int:id>', methods=['GET', 'POST'])
def edit(id):
    db = Database()
    if request.method == 'POST':
        date = request.form['datetime']
        desc = request.form['desc']
        if 'esp' in request.form:
            esp = request.form['esp']
        else:
            esp = None

        db.update('symptom_entries', id, {'date': date, 'esp': esp, 'desc': desc})

        # actualizar nombre
        symptom = dict(models.get_symptom_by_id(request.form['symptom_id']))
        db.update('symptoms', symptom['id'], {'name': request.form['symptom']})

        #actualizar tratamientos asociados
        db.delete('DELETE FROM symptom_treats WHERE entry_id=?', (id,))
        treats = request.form.getlist('treats')
        if len(treats) > 0:
            for t in treats:
                db.insert('symptom_treats', {'entry_id': id, 'treat_id': t})

        return redirect(url_for('index'))
    else:
        row, esp, treats, sel_treats = models.sym_form_info(id)
        selected = [x['treat_id'] for x in sel_treats]
        return render_template('edit.html', row=row, esp=esp, treats=treats, sel_treats=sel_treats, selected=selected)

@sym.route('/delete/<int:id>')
def delete(id):
    db=Database()
    db.delete("DELETE FROM symptom_entries WHERE id = ?", (id,))
    return redirect(url_for('index'))